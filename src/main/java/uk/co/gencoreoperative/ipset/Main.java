package uk.co.gencoreoperative.ipset;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import java.io.IOException;
import java.net.SocketException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class Main {
    @Parameter (description = "hostname")
    private List<String> hostnames = new ArrayList<String>();

    @Parameter (names = {"-e", "--etcHost"}, description = "Option override for etc hosts location", required = false)
    private String etcHost = "/etc/hosts";

    @Parameter (names = {"-h", "--help"}, description = "This help information", required = false)
    private boolean help = false;

    public String getHostname() {
        if (hostnames.isEmpty()) {
            error("Hostname is required");
        } else if (hostnames.size() > 1) {
            error("Only one hostname is permitted");
        }
        return hostnames.get(0);
    }

    public String getIP() {
        List<String> ips = Network.getAllIPs();
        if (ips.isEmpty()) {
            error("No interfaces found");
        }
        return ips.get(0);
    }

    public Etc getEtc() {
        return new Etc(etcHost);
    }

    public static void main(String... args) throws SocketException {

        Main main = new Main();
        JCommander jCommander = new JCommander(main, args);
        jCommander.setProgramName("ip-set");

        if (main.help) {
            byte[] bytes = new byte[0];
            try {
                bytes = Streams.readFully(Main.class.getResourceAsStream("/README"));
            } catch (IOException e) {
                error(e.getMessage());
            }
            System.out.println(new String(bytes));
            System.out.println();
            jCommander.usage();
            System.exit(0);
        }

        try {
            String ip = main.getIP();
            String hostname = main.getHostname();

            Etc etc = main.getEtc();
            etc.update(hostname, ip);

            System.out.println(MessageFormat.format(
                    "Updated {0} with {1} {2}",
                    etc.getHostsFileName(),
                    ip,
                    hostname));

            System.exit(0);
        } catch (IllegalStateException e) {
            System.err.println(MessageFormat.format("ERROR: {0}", e.getMessage()));
            System.exit(-1);
        }
    }

    private static void error(String msg, String...args) {
        throw new IllegalStateException(MessageFormat.format(msg, args));
    }
}