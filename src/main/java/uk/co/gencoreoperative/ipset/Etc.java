package uk.co.gencoreoperative.ipset;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Etc {
    private final String etcHost;

    public Etc(String etcHost) {
        this.etcHost = etcHost;
    }

    public String getHostsFileName() {
        return etcHost;
    }

    public void update(String hostname, String address) {
        List<String> etc = read(etcHost);
        replace(etc, hostname, address);
        write(etcHost, etc);
    }

    public static void replace(List<String> etc, String hostname, String address) {
        int index = Strings.search(etc, hostname);
        String entry = address + " " + hostname;
        if (index == -1) {
            etc.add(0, entry);
        } else {
            etc.set(index, entry);
        }
    }

    public static List<String> read(String path) {
        try {
            RandomAccessFile random = new RandomAccessFile(path, "rw");
            byte[] buf = new byte[(int) random.length()];
            random.readFully(buf);
            random.close();
            return new ArrayList<String>(Arrays.asList(new String(buf).split("\\r?\\n")));
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }

    public static void write(String path, List<String> etc) {
        try {
            RandomAccessFile random = new RandomAccessFile(path, "rw");
            random.seek(0);
            byte[] buf = Strings.join(etc).getBytes();
            random.setLength(buf.length);
            random.write(buf);
            random.close();
        } catch (IOException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }
}
