package uk.co.gencoreoperative.ipset;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Streams {
    public static byte[] readFully(InputStream stream) throws IOException {
        int read = 0;
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        byte[] buf = new byte[8000];
        while (read != -1) {
            read = stream.read(buf);
            if (read != -1) bout.write(buf, 0, read);
        }
        stream.close();
        return bout.toByteArray();
    }
}
