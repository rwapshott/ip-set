package uk.co.gencoreoperative.ipset;

import java.util.List;

public class Strings {
    private static final String NL = "\n";

    public static String join(List<String> l) {
        String r = ""; for (String e : l) r += e + NL;
        return r.isEmpty() ? r : r.substring(0, r.length() - NL.length());
    }

    public static int search(List<String> l, String keyword) {
        for (int ii = 0; ii < l.size(); ii++) {
            if (l.get(ii).contains(keyword)) return ii;
        }
        return -1;
    }
}
