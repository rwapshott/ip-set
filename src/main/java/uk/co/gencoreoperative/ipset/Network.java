package uk.co.gencoreoperative.ipset;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class Network {
    public static List<String> getAllIPs() {
        List<String> r = new ArrayList<String>();

        Enumeration<NetworkInterface> iter = null;
        try {
            iter = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e) {
            throw new IllegalStateException("Could not list network interfaces");
        }
        while (iter.hasMoreElements()) {
            NetworkInterface network = iter.nextElement();
            try {
                if (network.isLoopback()) continue;
            } catch (SocketException e) {
                throw new IllegalStateException(e.getMessage());
            }
            for (InterfaceAddress address : network.getInterfaceAddresses()) {
                InetAddress inetAddress = address.getAddress();
                if (inetAddress instanceof Inet6Address) continue;
                r.add(inetAddress.getHostAddress());
            }
        }
        return r;
    }
}
